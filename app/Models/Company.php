<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'cnpj',
        'trading_name',
        'corporate_name',
    ];

    public static function store(array $data) : Company {
        $company =  new Company();
        $company->fill($data);
        $company->save();

        return $company;
    }
    
    public static function edit(array $data) : Company {
        $company =  Company::findOrFail($data['id']);
        $company->fill($data);
        $company->save();

        return $company;
    }

    protected function cnpj(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => Crypt::decryptString($value),
            set: fn (string $value) => Crypt::encryptString($value),
        );
    }

    public static function validateCnpj(int $cnpj){
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);	
        
        if (strlen($cnpj) != 14)
            return false;

        
        if (preg_match('/(\d)\1{13}/', $cnpj))
            return false;	
        
        for ($i = 0, $j = 5, $sum = 0; $i < 12; $i++) {
            $sum += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $remainder = $sum % 11;

        if ($cnpj[12] != ($remainder < 2 ? 0 : 11 - $remainder))
            return false;

        for ($i = 0, $j = 6, $sum = 0; $i < 13; $i++) {
            $sum += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $remainder = $sum % 11;

        return $cnpj[13] == ($remainder < 2 ? 0 : 11 - $remainder);
    }
}
