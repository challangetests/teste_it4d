<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Expense extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'is_approved',
        'approver_id', 
        'approval_date', 
        'refuse_reason',
        'user_id',
        'company_id'
    ];


    public static function store(array $data) : Expense {
        $expense =  new Expense();
        $expense->fill($data);
        $expense->save();

        return $expense;
    }
    
    public static function edit(array $data) : Expense {
        $expense =  Expense::findOrFail($data['id']);
        $expense->fill($data);
        $expense->save();

        return $expense;
    }
    
    public static function findByApprover(array $data) : ?Expense {
        return Expense::where('id', $data['id'])->whereNull('is_approved')
            ->where('approver_id', $data['approver_id'])->first();   
    }

    public function user(): HasOne {
        return $this->hasOne(User::class, 'id', 'user_id');
    } 
    
    public function approver(): HasOne {
        return $this->hasOne(User::class, 'id', 'approver_id');
    } 
    
    public function company(): HasOne {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
