<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseGroupeduserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $expensesSum = $this->expense()->where('is_approved', 1)->sum('value');
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "cpf" => $this->cpf,
            "company" => new CompanyResource($this->company),
            "expenses" => number_format($expensesSum, 2, '.' , '')
        ];
    }
}
