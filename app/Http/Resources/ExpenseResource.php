<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "value" => $this->value,
            "is_approved" => $this->is_approved,
            "approval_date" => $this->approval_date,
            "refuse_reason" => $this->refuse_reason,
            "approver" => new UserResource($this->approver),
            "user" => new UserResource($this->user),
            "company" => new CompanyResource($this->company),
        ];
    }
}
