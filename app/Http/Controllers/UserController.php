<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function store(Request $request){
        try {
            $request->validate([
                'cpf' => 'required|integer|digits:11',
                'name' => 'required|string|max:255',
                'email' => 'required|email|unique:users|max:255',
                'password' => 'required|email|max:255',
                'company_id' => 'required|exists:companies,id',
            ]);
            $data = $request->all();

            if (!User::validateCpf($data['cpf'])) {
                return response()->json(
                    ["errors" => ["cpf" => [__('validation.cpf')]]],
                Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            DB::beginTransaction();
            $user = User::store($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.insert', ["model" => __('crud.model.user')]),
                "user" => new UserResource($user)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function show(int $id){
        $users = User::findOrFail($id);

        return  response()->json([
            "users" => new UserResource($users)
        ]);
    }
    
    public function index(){
        $users = User::all();

        return  response()->json([
            "users" => UserResource::collection($users)
        ]);
    }

    public function update(Request $request, int $id){
        try {
            $request->validate([
                'cpf' => 'required|integer|digits:11',
                'name' => 'required|string|max:255',
                'email' => 'email|unique:users|max:255',
                'password' => 'required|email|max:255',
                'company_id' => 'required|exists:companies,id',
            ]);
            $data = $request->all();
            $data['id'] = $id;

            if (isset($data['cpf'])) {
                if (!User::validateCpf($data['cpf'])) {
                    return response()->json(
                        ["errors" => ["cpf" => [__('validation.cpf')]]],
                    Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            DB::beginTransaction();
            $user = User::edit($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.update', ["model" => __('crud.model.user')]),
                "user" => new UserResource($user)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(int $id){
        try {
            DB::beginTransaction();
            User::findOrFail($id)->delete();
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.delete', ["model" => __('crud.model.user')]),
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
