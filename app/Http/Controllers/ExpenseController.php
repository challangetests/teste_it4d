<?php

namespace App\Http\Controllers;

use App\Http\Resources\ExpenseGroupeduserResource;
use App\Http\Resources\ExpenseResource;
use App\Models\Expense;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ExpenseController extends Controller
{
    public function store(Request $request){
        try {
            $request->validate([
                'value' => 'required|numeric|decimal:0,2,',
                'approver_id' => 'required|integer|exists:users,id',
                'user_id' => 'required|integer|exists:users,id',
                'company_id' => 'required|integer|exists:companies,id',
            ]);
            $data = $request->all();

            DB::beginTransaction();
            $expense = Expense::store($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.insert', ["model" => __('crud.model.expense')]),
                "expense" => new ExpenseResource($expense)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function show(int $id){
        $expense = Expense::findOrFail($id);

        return  response()->json([
            "expense" => new ExpenseResource($expense)
        ]);
    }
    
    public function index(){
        $expenses = Expense::all();

        return  response()->json([
            "expenses" => ExpenseResource::collection($expenses)
        ]);
    }
    
    public function indexGroupedByUser(){
        $users = User::all();

        return  response()->json([
            "users" => ExpenseGroupeduserResource::collection($users)
        ]);
    }

    public function update(Request $request, int $id){
        try {
            $request->validate([
                'value' => 'required|numeric|decimal:0,2,',
                'approver_id' => 'required|integer|exists:users,id',
                'user_id' => 'required|integer|exists:users,id',
                'company_id' => 'required|integer|exists:companies,id',
            ]);
            $data = $request->all();
            $data['id'] = $id;

            DB::beginTransaction();
            $expense = Expense::edit($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.update', ["model" => __('crud.model.expense')]),
                "expense" => new ExpenseResource($expense)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    
    public function approval(Request $request, int $id){
        try {
            $request->validate([
                'is_approved' => 'required|boolean',
                'refuse_reason' => 'required_if:is_approved,false|string|max:255'
            ]);

            $data = $request->all();
            $data['id'] = $id;
            $data['approver_id'] = $request->user()->id;

            DB::beginTransaction();
            $expense = Expense::findByApprover($data);
            if (is_null($expense)) {
                throw new ModelNotFoundException();
            }
            $data['approval_date'] = date('Y-m-d');
            $expense = Expense::edit($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.approved'),
                "expense" => new ExpenseResource($expense)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(int $id){
        try {
            DB::beginTransaction();
            Expense::findOrFail($id)->delete();
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.delete', ["model" => __('crud.model.expense')]),
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
