<?php

namespace App\Http\Controllers;

use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CompanyController extends Controller
{
    public function store(Request $request){
        try {
            $request->validate([
                'cnpj' => 'required|integer|digits:14',
                'trading_name' => 'required|string|max:255',
                'corporate_name' => 'required|string|max:255',
            ]);
            $data = $request->all();

            if (!Company::validateCnpj($data['cnpj'])) {
                return response()->json(
                    ["errors" => ["cnpj" => [__('validation.cnpj')]]],
                Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            DB::beginTransaction();
            $company = Company::store($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.insert', ["model" => __('crud.model.company')]),
                "company" => new CompanyResource($company)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function show(int $id){
        $company = Company::findOrFail($id);

        return  response()->json([
            "company" => new CompanyResource($company)
        ]);
    }
    
    public function index(){
        $companies = Company::all();

        return  response()->json([
            "companies" => CompanyResource::collection($companies)
        ]);
    }

    public function update(Request $request, int $id){
        try {
            $request->validate([
                'cnpj' => 'required|integer|digits:14',
                'trading_name' => 'required|string|max:255',
                'corporate_name' => 'required|string|max:255',
            ]);
            $data = $request->all();
            $data['id'] = $id;

            if (isset($data['cnpj'])) {
                if (!Company::validateCnpj($data['cnpj'])) {
                    return response()->json(
                        ["errors" => ["cnpj" => [__('validation.cnpj')]]],
                    Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            DB::beginTransaction();
            $company = Company::edit($data);
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.update', ["model" => __('crud.model.company')]),
                "company" => new CompanyResource($company)
            ]);
        }
        catch (ValidationException $e) {
            return response()->json(
                ["errors" => $e->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(int $id){
        try {
            DB::beginTransaction();
            Company::findOrFail($id)->delete();
            DB::commit();

            return  response()->json([
                "message" => __('crud.message.delete', ["model" => __('crud.model.company')]),
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            throw $e;
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
