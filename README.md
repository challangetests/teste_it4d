## Instalando o projeto

Para subir e utilizar o projeto com usuários autenticados pelo laravel passport utilize os seguintes passos

- ``` php artisan migrate``` Para criar as tabelas
- ``` php artisan db:seed``` Para gerar usuarios de teste
- ```php artisan passport:client --password``` Para criar um cliente de autenticação que usa usuário e senha

## Obtendo um token

Para conseguir um token de autenticação requisite a rota ```POST /oauth/token``` com os seguintes dados:

- username: user.email@example.net -- email gerado pelo seeder de usuarios
- password: password --senha padrão do seeder
- client_id: 1 -- dados do cliente gerado pelo comando passport:client
- client_secret:auM2EL9t9RPK1TSjUA0JbNFqVmgsGLPykzG80t3P
- grant_type:password

O retorno ira devolver o valor "access_token" com o token, use-o nas rotas protegidas como bearer token para ter acesso.

## Cadastrando Despesas

Para cadastrar uma despesa use o endpoint ```POST /api/expense/``` com os seguintes dados:

- user_id: 1 -- ID do usuario da despesa.    
- approver_id: 5 -- ID do usuario aprovador da despesa.
- company_id: 1 -- ID da empresa da despesa
- value:500.99 -- valor da despesa

As despesas possuem 3 status de acordo com o campo "is_approved":

- null : Despesa pendente de aprovação, status padrão de criação.
- true : Despesa Aprovada
- false : Despesa reprovada

## Aprovando Despesas

Para aprovar uma despesa use o endpoint ```PUT /api/expense/{id}/approval``` com os seguintes dados:

- {id} : ID da despesa a ser aprovada.
- Token : Somente o approvador pode aprovar a despesa ele é identificado pelo token enviado.
- is_approved: (1/0)  Status de aprovado ou reprovado.
- refuse_reason: Obrigatório quando se esta reprovando uma despesa.

## Agrupamento de Despesas

Para consultar a soma de despesas aprovadas de cada usuário use o endpoint ```GET /api/expense/group-user```;
