<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'model' => [
        "company" => "Company",
        "user" => "User",
        "expense" => "Expense",
    ],
    'message' => [
        "insert" => ":model created successfully.",
        "update" => ":model updated successfully.",
        "delete" => ":model deleted successfully.",
        "not_found" => "Object not found.",
        "approved" => "Approval done successfully.",
    ],

];
